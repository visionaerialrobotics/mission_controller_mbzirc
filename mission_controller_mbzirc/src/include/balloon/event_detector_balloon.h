#ifndef EVENT_DETECTOR_BALLOON_H
#define EVENT_DETECTOR_BALLOON_H

#include <iostream>
#include "state_machine.h"
#include <math.h>
#include "eigen3/Eigen/Eigen"
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Point.h>
#include <chrono>
#include <ctime>
#include "state_machine_balloon.h"

class EventDetectorBalloon
{

public:

  //Constructor.
  EventDetectorBalloon();

  // Destructor.
  ~EventDetectorBalloon();

  void setPoses(geometry_msgs::PoseStamped ball_pose,
                geometry_msgs::PoseStamped base_pose);
  void setTime(double time_ball);
  void setGripperOffset(Eigen::Vector3d gripperOffset);
  bool generateEvent(ItemBalloon::State current_state, ItemBalloon::Event & next_event);
  void printEvent(ItemBalloon::Event event);
  void setCounterCloseBallDetector(int counter);

  void startTimerRecover();
  void setTimeRecoverThreshold(int counter);

  void activateBallDetectionAllTime(bool activate_ball_detector_all_time);
  int n_min_ball_close_detections;
  int burst_counter_;
  bool wait_key_take_off;

  double distanceBetweenPoses(geometry_msgs::PoseStamped pose_1, geometry_msgs::PoseStamped pose_2);

  double min_height_take_off;
  geometry_msgs::PoseStamped center_search_behaviour;

  int exploration_points_size_;
  void setExplorationPointsSize(int num);
  int  current_traj_num_;
  void setCurrentTrajNum(int num);
  geometry_msgs::PoseStamped current_exploration_point_;
  bool reached_last_burst_point_;
  void setCurrentExplorationPoint(geometry_msgs::PoseStamped pose, bool reached_last_burst_pose);

private:

  bool ACTIVATE_BALL_DETECTOR_ALL_TIME;

  ItemBalloon::Event last_event;

  geometry_msgs::PoseStamped ball_pose;
  geometry_msgs::PoseStamped base_pose;

  //motion_controller_process::SetPoints planner_points;
  Eigen::Vector3d gripperOffset;

  double time_ball;

  double initial_time_recover;
  double time_elapsed_recover_threshold;

  int counter_ball_close;
};
#endif
