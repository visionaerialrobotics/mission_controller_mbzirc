#include "balloon/mission_controller_balloon_mbzirc_process.h"

MissionControllerBalloonProcess::MissionControllerBalloonProcess(){
    state_machine = StateMachineBalloon();
    behavior_execution = BehaviorExecutionBalloon();
    event_detector = EventDetectorBalloon();
}

MissionControllerBalloonProcess::~MissionControllerBalloonProcess(){}

double MissionControllerBalloonProcess::get_moduleRate(){
    return rate;
}

void MissionControllerBalloonProcess::ownSetUp()
{   // Configs
    ros::param::get("~frequency", rate);
    ros::param::get("~robot_namespace", drone_id_namespace);
    ros::param::get("~time_threshold", time_threshold);

    //Topics
    ros::param::get("~planner_setpoints_topic", planner_setpoints_topic);
    ros::param::get("~detector_mode_topic", detector_mode_topic);
    ros::param::get("~external_yaw_topic", external_yaw_topic);
    ros::param::get("~high_level_command_topic", high_level_command_topic);

    //States timeouts

    //Detector
    ros::param::get("~n_min_ball_close_detections", n_min_ball_close_detections);

    //x and y area
    ros::param::get("~x_area", x_area_);
    ros::param::get("~y_area", y_area_);
    ros::param::get("~x_arena_low_limit", x_arena_low_limit_);
    ros::param::get("~x_arena_high_limit", x_arena_high_limit_);
    ros::param::get("~y_arena_low_limit", y_arena_low_limit_);
    ros::param::get("~y_arena_high_limit", y_arena_high_limit_);
    ros::param::get("~z_min", z_min_);
    ros::param::get("~z_max", z_max_);

    std::cout << "x area around balloon: " << x_area_ << std::endl;
    std::cout << "y area around balloon: " << y_area_ << std::endl;
    std::cout << "x arena low limit " << x_arena_low_limit_ << std::endl;
    std::cout << "x arena high limit " << x_arena_high_limit_ << std::endl;
    std::cout << "y arena low limit " << y_arena_low_limit_ << std::endl;
    std::cout << "y arena high limit " << y_arena_high_limit_ << std::endl;
    std::cout << "z min " << z_min_ << std::endl;
    std::cout << "z max " << z_max_ << std::endl;

    double y_offset, front_x_offset, balloon_altitude, exploration_altitude, back_x_offset, exploration_yaw;
    ros::param::get("~y_offset", y_offset);
    ros::param::get("~front_x_offset", front_x_offset);
    ros::param::get("~balloon_altitude", balloon_altitude);
    ros::param::get("~exploration_altitude", exploration_altitude);
    ros::param::get("~back_x_offset", back_x_offset);
    ros::param::get("~exploration_yaw", exploration_yaw);

    behavior_execution.setYaxisOffset(y_offset);
    behavior_execution.setFrontXaxisOffset(front_x_offset);
    behavior_execution.setBackXaxisoffset(back_x_offset);
    behavior_execution.setBalloonAltitude(balloon_altitude);
    behavior_execution.setExplorationAltitude(exploration_altitude);
    behavior_execution.setExplorationYaw(exploration_yaw);

    //reading the txt file of exploration points
    ros::param::get("~text_file", text_file_);
    std::cout << "text file location: " << text_file_ << std::endl;

    std::ifstream infile(text_file_);
    if(!infile.is_open())
    {
        std::cerr << "cannot open the exploration points file " << std::endl;
    }

    this->readTextFile(text_file_);

    //TODO
    int RATIO_FOR_FOCUS_FAR = 3;
    event_detector.n_min_ball_close_detections = n_min_ball_close_detections;
    //event_detector.min_height_take_off = min_altitude_external_altitude_control;

    prev_ball_pose.pose.position.x = 0;
    prev_ball_pose.pose.position.y = 0;
    prev_ball_pose.pose.position.z = 0;
}

void MissionControllerBalloonProcess::readTextFile(std::string txt_file){
    std::vector<geometry_msgs::Point> exploration_points_vec;
    geometry_msgs::Point single_exploration_point;
    std::string line;

    std::ifstream infile(txt_file);
    while(std::getline(infile, line))
    {
        std::string x, y,z;
        std::stringstream ss(line);
        std::getline(ss, x, ',');
        single_exploration_point.x = std::stof(x);
        std::getline(ss, y, ',');
        single_exploration_point.y = std::stof(y);
        std::getline(ss, z, ',');
        single_exploration_point.z = std::stof(z);

        exploration_points_vec.push_back(single_exploration_point);
    }

    for(int i =0; i < exploration_points_vec.size(); ++i)
    {
        std::cout << "Exploration points: " << exploration_points_vec[i].x << "," <<
                     exploration_points_vec[i].y << "," <<
                     exploration_points_vec[i].z << std::endl;
    }
    behavior_execution.setVectorOfExplorationPoints(exploration_points_vec);
    event_detector.setExplorationPointsSize(exploration_points_vec.size());

}

void MissionControllerBalloonProcess::ownStart(){

    ros::NodeHandle n;
    std::cout << std::endl << std::endl << "MISSION CONTROLLER STATES SEQUENCE"<< std::endl;
    std::cout << "----------------------------------"<< std::endl;
    current_state = ItemBalloon::State::START_STATE;
    state_machine.printState(current_state);
    //event = -1;
    successfull_execution = true;

    //time_threshold = 2;
    counter_ball_close = 0;

    far_counter_enable_flag = false;
    close_counter_enable_flag = false;
    first_burst_center_point_ = false;

    state_machine.activateBallDetectionAllTime(true);
    event_detector.activateBallDetectionAllTime(true);

    ball_pose.pose.position.x = 0.0;
    ball_pose.pose.position.y = 0.0;
    ball_pose.pose.position.z = 3.0;

    time_target_drone_far = ros::Time::now().toSec();
    time_target_drone_close = ros::Time::now().toSec();
    time_ball = ros::Time::now().toSec();

    external_yaw_publisher =  n.advertise<nav_msgs::Odometry>(external_yaw_topic, 1);
    planner_publisher      =  n.advertise<mission_controller_mbzirc_process::SetPoints>(planner_setpoints_topic, 1);
    mpc_points_publisher   =  n.advertise<geometry_msgs::PoseStamped>("command/pose",1);
    high_level_command_publisher =  n.advertise<droneMsgsROS::droneCommand>(high_level_command_topic, 1);

    detector_mode_subscriber = n.subscribe("/"+drone_id_namespace+"/detector_mode", 1, &MissionControllerBalloonProcess::detectorModeCallback, this);

    //Commands from visualizer
    command_subscriber = n.subscribe("mission_visualizer/command", 1, &MissionControllerBalloonProcess::commandsCallback, this);

    //Visualizer
#ifdef UseVisualizer
    target_drone_far_publisher =  n.advertise<geometry_msgs::PoseStamped>("/"+drone_id_namespace+"/mission_visualizer/target_far_drone", 1);
    target_drone_close_publisher =  n.advertise<geometry_msgs::PoseStamped>("/"+drone_id_namespace+"/mission_visualizer/target_close_drone", 1);
    counter_target_far_publisher =  n.advertise<std_msgs::Int32>("/"+drone_id_namespace+"/mission_visualizer/counter_far", 1);
    counter_target_close_publisher =  n.advertise<std_msgs::Int32>("/"+drone_id_namespace+"/mission_visualizer/counter_close", 1);
    counter_ball_publisher =  n.advertise<std_msgs::Int32>("/"+drone_id_namespace+"/mission_visualizer/counter_ball", 1);
    current_state_publisher =  n.advertise<std_msgs::Int32>("/"+drone_id_namespace+"/mission_visualizer/state", 1);
    last_event_publisher =  n.advertise<std_msgs::Int32>("/"+drone_id_namespace+"/mission_visualizer/event", 1);
#endif

}

void MissionControllerBalloonProcess::ownStop(){

}

void MissionControllerBalloonProcess::ownRun()
{

    //Get variables from TF
    getVariablesFromTF();
    //get current traj number
    int num;
    behavior_execution.getCurrentTrajectoryNum(num);
    event_detector.setCurrentTrajNum(num);
    geometry_msgs::PoseStamped pose; bool reached_last_burst_point = false;
    behavior_execution.getCurrentExplorationPoint(pose, reached_last_burst_point);
    event_detector.setCurrentExplorationPoint(pose, reached_last_burst_point);

    //Generate event
    if (event_detector.generateEvent(current_state, event)){
        //Next state
        state_machine.nextState(current_state,event);
    }

#ifdef UseVisualizer
    std_msgs::Int32 msg;
    msg.data = (int)current_state;
    current_state_publisher.publish(msg);
    msg.data = (int)event;
    last_event_publisher.publish(msg);
#endif

    //Returns:
    // 0 data does not need to be sent
    // 1 planner goal points need to be sent
    // 2 external yaw need to be sent
    // 3 planner goal and external yaw need to be sent
    // 4 take off high level command needs to be sent
    // 5 land high level command needs to be sent
    //Execute State
    switch(behavior_execution.executeState(current_state,points_list,inflation_radius,gear_speed, external_yaw)){
    case 0:
        //Nothing to do

        break;
    case 1:
        publishPoints(points_list,inflation_radius,gear_speed);

        break;
    case 2:
        publishExternalYaw(external_yaw, current_state);
        break;
    case 3:
        publishPoints(points_list,inflation_radius,gear_speed);
        publishExternalYaw(external_yaw, current_state);

        break;
    case 4:
        takeOffPublisher();
        publishPoints(points_list,inflation_radius,gear_speed);

        break;
    case 5:
        landPublisher();
        break;
    }
    behavior_execution.resetFlags(current_state);

}

// Get ball, target drone and base drone poses and gripper offset from TF, return false if a variable couldn't be gotten
void MissionControllerBalloonProcess::getVariablesFromTF(){

    getPoseFromTF("/base_link",base_link_pose);
    behavior_execution.setBaseLinkPose(base_link_pose);
    getPoseFromTF("/ball_close",ball_pose);
    event_detector.setPoses(ball_pose, base_link_pose);
		checkTimeOutCounterTF("/ball_close", counter_ball_close);			

    if(this->insideArena(ball_pose)){

        if(current_state == ItemBalloon::State::BURST_BALLOON)
        {
            if(!first_burst_center_point_){
                burst_center_point_ = ball_pose;
                first_burst_center_point_ = true;
                behavior_execution.setBallPose(ball_pose);
            }
            else if(this->insideAllowedArea(ball_pose, burst_center_point_)){
                behavior_execution.setBallPose(ball_pose);
            }
        }
        else if(current_state == ItemBalloon::State::SEARCH_BALLOON)
        {
            first_burst_center_point_ = false;
            behavior_execution.setBallPose(ball_pose);

        }
        
        if(getTimeFromTF("/ball_close", time_ball)){
            behavior_execution.setBallTime(time_ball);
        }

        //event_detector.setPoses(ball_pose, base_link_pose);

#ifdef UseVisualizer
        target_drone_far_publisher.publish(drone_far_pose);
        target_drone_close_publisher.publish(drone_close_pose);
        std_msgs::Int32 msgInt;
        msgInt.data = 0;
        counter_target_far_publisher.publish(msgInt);
        msgInt.data = 0;
        counter_target_close_publisher.publish(msgInt);
        msgInt.data = counter_ball_close;
        counter_ball_publisher.publish(msgInt);
#endif
    }
    else {
        //ROS_ERROR("DETECTED FALSE POSITIVE OUTSIDE THE ARENA");
        //std::cout << "balloon pose:" << ball_pose.pose.position.x << "," << ball_pose.pose.position.y << "," << ball_pose.pose.position.z << std::endl;					
				counter_ball_close = counter_ball_close<1?0:counter_ball_close-1;        
				return;
    }

		event_detector.setCounterCloseBallDetector(counter_ball_close);

}

//Get pose frame from tf with odom
bool MissionControllerBalloonProcess::getPoseFromTF(std::string frame, geometry_msgs::PoseStamped & pose){
    tf::StampedTransform transform;
    try{
        listener.lookupTransform("odom", frame, ros::Time(0), transform);

        double x = transform.getOrigin().x();
        double y = transform.getOrigin().y();
        double z = transform.getOrigin().z();

        if(!(std::isfinite(x) && std::isfinite(y) && std::isfinite(z))){
            //first_detection_flag = false;
            ROS_WARN(" * * * * TF VALUE IS NAN  * * * * ");
            return false;
        }else{
            pose.pose.position.x = x;
            pose.pose.position.y = y;
            pose.pose.position.z = z;
            return true;
        }
    }catch (tf::TransformException ex){
        //ROS_ERROR("%s",ex.what());
        return false;
    }
}

//Get last time the frame was detected
bool MissionControllerBalloonProcess::getTimeFromTF(std::string frame, double & time){
    tf::StampedTransform transform;
    try{
        listener.lookupTransform("odom", frame, ros::Time(0), transform);
        //Avoid getting same frame than before
        if (time == transform.stamp_.toSec()){
            return false;
        }else{
            time = transform.stamp_.toSec();
        }
        return true;
    }catch (tf::TransformException ex){
        //ROS_ERROR("%s",ex.what());
        return false;
    }
}

//Get sequence of times that target drone has been seen
void MissionControllerBalloonProcess::checkTimeOutCounterTF(std::string frame, int & counter){
    tf::StampedTransform transform;
    try{
        listener.lookupTransform("odom", frame, ros::Time(0), transform);
        double tf_time = transform.stamp_.toSec();
        double time_now = ros::Time::now().toSec();
        //        std::cout << "----------------------------------"<< std::endl;
        //        std::cout << "frame : " << frame << std::endl;
        //        std::cout << "time_threshold : " << time_threshold << std::endl;
        //        std::cout << "abs(tf_time-time_now : " << fabs(tf_time-time_now)<< std::endl;
        if ((fabs(tf_time-time_now) > time_threshold) && (counter > 0)){

            std::cout << "************COUNTER RESET**********************"<< std::endl;
            std::cout << "************"<< frame <<"************"<< std::endl;

            counter = 0;
        }
				else
					counter_ball_close++;


    }catch (tf::TransformException ex){
        //ROS_ERROR("%s",ex.what());
    }
}

//Publish points
void MissionControllerBalloonProcess::publishPoints(std::vector<geometry_msgs::PoseStamped> points_list, float inflation_radius, float  gear_speed){
    mission_controller_mbzirc_process::SetPoints msg;


    msg.points_list = points_list;
    msg.inflation_radius = inflation_radius;
    msg.gear_speed = gear_speed;
    //planner_publisher.publish(msg);
    //sending only the first point not a bug
    mpc_points_publisher.publish(points_list[0]);
}

void MissionControllerBalloonProcess::publishExternalYaw(nav_msgs::Odometry external_yaw, ItemBalloon::State state){
    double SLOW_K_YAW = 0.4;
    double FAST_K_YAW = 1.4;

    external_yaw.pose.pose.orientation.x = FAST_K_YAW;

    if(state == ItemBalloon::State::SEARCH_BALLOON){
        external_yaw.pose.pose.orientation.x = SLOW_K_YAW;
    }
    external_yaw_publisher.publish(external_yaw);
}


//Detector mode callback
void MissionControllerBalloonProcess::detectorModeCallback(const std_msgs::Int32::ConstPtr &msg){

    //if (msg->data == -1){
        //counter_ball_close += 1;
    //}
}

//Command callback from visualizer
void MissionControllerBalloonProcess::commandsCallback(const std_msgs::Int32::ConstPtr &msg){
    // 0: Stop and reset
    // 1: Take off
    // 2: Land
    // 3: Next state
    // 4: Previous state
    switch(msg->data){
    case 0:
        std::cout << "STOP AND RESET"<< std::endl;
        break;
    case 1:
        std::cout << "TAKE OFF"<< std::endl;
        event_detector.wait_key_take_off = true;
        break;
    case 2:
        std::cout << "LAND"<< std::endl;
        current_state = ItemBalloon::State::LAND;
        break;
    case 3:
        std::cout << "NEXT_STATE"<< std::endl;
        break;
    case 4:
        std::cout << "PREVIOUS_STATE"<< std::endl;
        break;
    }
}

void MissionControllerBalloonProcess::takeOffPublisher(){
    droneMsgsROS::droneCommand msg;
    msg.command = droneMsgsROS::droneCommand::TAKE_OFF;
    high_level_command_publisher.publish(msg);
    ROS_WARN(" * * * * DRONE IS TAKING OFF  * * * * ");
}

void MissionControllerBalloonProcess::landPublisher(){
    droneMsgsROS::droneCommand msg;
    msg.command = droneMsgsROS::droneCommand::LAND;
    high_level_command_publisher.publish(msg);
    ROS_WARN(" * * * * DRONE IS LANDING  * * * * ");

}

void MissionControllerBalloonProcess::transformVectorStringToDouble(std::string & input, geometry_msgs::PoseStamped & output){
    boost::erase_all(input, " ");
    boost::erase_all(input, "[");
    boost::erase_all(input, "]");

    std::vector<std::string> tmp_vector;
    boost::split ( tmp_vector, input, boost::is_any_of(","));
    std::vector<double> tmp_vector_double;

    for(auto it = tmp_vector.begin(); it != tmp_vector.end() ; it ++ ){
        tmp_vector_double.push_back(stod(*it));
        //std::cout<< *it<<std::endl;


    }
    output.pose.position.x = tmp_vector_double[0];
    output.pose.position.y = tmp_vector_double[1];
    output.pose.position.z = tmp_vector_double[2];

}

void MissionControllerBalloonProcess::dynamicTunningCallback(mission_controller_mbzirc::MyParamsConfig &config, uint32_t level){

}

bool MissionControllerBalloonProcess::insideAllowedArea(geometry_msgs::PoseStamped ball_pose, geometry_msgs::PoseStamped first_burst_pose){

    if(fabs(ball_pose.pose.position.x - first_burst_pose.pose.position.x) < x_area_ &&
            fabs(ball_pose.pose.position.y - first_burst_pose.pose.position.y) < y_area_){
        return true;
    }
    else {
        return false;
    }
}

bool MissionControllerBalloonProcess::insideArena(geometry_msgs::PoseStamped ball_pose)
{
    if(ball_pose.pose.position.x < x_arena_low_limit_ || ball_pose.pose.position.x > x_arena_high_limit_ ||
            ball_pose.pose.position.y < y_arena_low_limit_ || ball_pose.pose.position.y > y_arena_high_limit_ ||
            ball_pose.pose.position.z < z_min_ || ball_pose.pose.position.z > z_max_){
        return false;
    }
    else {
        return true;
    }
}
