#include "balloon/mission_visualizer_balloon_process_main.h"

//Callback Functions
void targetFarDronePoseCallback(const geometry_msgs::PoseStamped::ConstPtr &msg) {
    target_far_pose = *msg; 
}

void targetCloseDronePoseCallback(const geometry_msgs::PoseStamped::ConstPtr &msg) {
    target_close_pose = *msg; 
}

void counterTargetFarCallback(const std_msgs::Int32::ConstPtr &msg){
    if (msg->data != counter_far_consecutive_detections){    
        counter_far_consecutive_detections= msg->data;
        counter_far_time = ros::Time::now().toSec();
    }
}
void counterTargetCloseCallback(const std_msgs::Int32::ConstPtr &msg){
    if (msg->data != counter_close_consecutive_detections){    
        counter_close_consecutive_detections = msg->data;
        counter_close_time = ros::Time::now().toSec();
    }
}
void counterBallCallback(const std_msgs::Int32::ConstPtr &msg){
    if (msg->data != counter_ball_consecutive_detections){    
        counter_ball_consecutive_detections = msg->data;
        counter_ball_time = ros::Time::now().toSec();
    }
}

void eventCallback(const std_msgs::Int32::ConstPtr &msg){
    if (msg->data != last_event){
        last_event2 = last_event1;
        last_event1 = last_event;
        last_event = msg->data;
    }
}
void stateCallback(const std_msgs::Int32::ConstPtr &msg){
    if (msg->data != current_state){
        past_state3 = past_state2;
        past_state2 = past_state1;
        past_state1 = current_state;
        current_state = msg->data;        
    }
}

void batteryCallback(const sensor_msgs::BatteryState::ConstPtr& msg){ 
    battery_msg=*msg;
}

void odometryCallback(const nav_msgs::Odometry::ConstPtr &msg){
    base_drone_pose = *msg;
    tf2::Matrix3x3 m(tf2::Quaternion 
        (base_drone_pose.pose.pose.orientation.x,
            base_drone_pose.pose.pose.orientation.y,
            base_drone_pose.pose.pose.orientation.z,
            base_drone_pose.pose.pose.orientation.w));
    
    m.getRPY(roll, pitch, yaw);
    if (std::isnan(yaw)) yaw = 0.0;
}

int main(int argc, char **argv)
{
    //OUTPUT FORMAT
    interface_printout_stream << std::fixed << std::setprecision(2) << std::setfill('0'); //<< std::showpos

    //ROS 
    ros::init(argc, argv, "mission_visualizer_process");
    ros::NodeHandle n;

    //Configuration
    ros::param::get("~robot_namespace", drone_id_namespace);
    ros::param::get("~frequency", frequency);
    ros::param::get("~mission_time_seconds", mission_time_seconds);

    //Subscribers
    target_far_pose_sub =  n.subscribe<geometry_msgs::PoseStamped>("/"+drone_id_namespace+"/mission_visualizer/target_far_drone", 1, &targetFarDronePoseCallback);
    target_close_pose_sub =  n.subscribe<geometry_msgs::PoseStamped>("/"+drone_id_namespace+"/mission_visualizer/target_close_drone", 1, &targetCloseDronePoseCallback);
    counter_target_far_sub =  n.subscribe<std_msgs::Int32>("/"+drone_id_namespace+"/mission_visualizer/counter_far", 1, &counterTargetFarCallback);
    counter_target_close_sub =  n.subscribe<std_msgs::Int32>("/"+drone_id_namespace+"/mission_visualizer/counter_close", 1, &counterTargetCloseCallback);
    counter_ball_sub =  n.subscribe<std_msgs::Int32>("/"+drone_id_namespace+"/mission_visualizer/counter_ball", 1, &counterBallCallback);
    current_state_sub =  n.subscribe<std_msgs::Int32>("/"+drone_id_namespace+"/mission_visualizer/state", 1, &stateCallback);
    last_event_sub =  n.subscribe<std_msgs::Int32>("/"+drone_id_namespace+"/mission_visualizer/event", 1, &eventCallback);
    drone_pose_sub =  n.subscribe<nav_msgs::Odometry>("/msf_core/odometry", 1, &odometryCallback);
    battery_sub=n.subscribe("/"+drone_id_namespace+"/dji_sdk/battery_state", 1, &batteryCallback);
    //Publishers
    command_pub = n.advertise<std_msgs::Int32>("/"+drone_id_namespace+"/mission_visualizer/command", 1);

    //Initialization
    counter_far_consecutive_detections = 0;counter_far_time = 0;
    counter_close_consecutive_detections = 0;counter_close_time = 0;
    counter_ball_consecutive_detections = 0;counter_ball_time = 0;
    last_event2 = -1;last_event1 = -1;past_state3 = -1;past_state2 = -1;past_state1 = -1;
    roll = 0; pitch = 0; yaw = 0;
    mission_started = false;
    //ncurses initialization (output text)
    initscr();
    start_color();
    use_default_colors();  
    curs_set(0);
    noecho();
    nodelay(stdscr, TRUE);
    erase();
    refresh();
    init_pair(1, COLOR_GREEN, -1);
    init_pair(2, COLOR_RED, -1);
    init_pair(3, COLOR_CYAN, -1);
    init_pair(4, COLOR_YELLOW, -1);

    //Input variable
    char command = 0;
    int command_confirm = -1;

    printStaticMenu();
    
    //Rate
    ros::Rate loop_rate(frequency);

    mission_time = ros::Time::now().toSec();

    //Loop
    while (ros::ok()) {
        
        //Read messages
        ros::spinOnce();

        //Time remaining and battery
        move(2,72);
        if(mission_started){
            int aux = mission_time_seconds - (ros::Time::now().toSec() - mission_time);
            if (aux <= 0){    
                attron(COLOR_PAIR(2));printTime(0);attroff(COLOR_PAIR(2)); 
            }else{
                printTime(aux);                   
            }
        }else printTime(mission_time_seconds);

        printBattery();printw(" %%  ");
        //States
        move(3,5);
        attron(COLOR_PAIR(1));printState(current_state);attroff(COLOR_PAIR(1));
        move(4,5);
        printState(past_state1);
        move(5,5);
        printState(past_state2);
        move(6,5);
        printState(past_state3);

        //Events
        move(3,34);
        printw("-");
        move(4,30);
        attron(COLOR_PAIR(3));printEvent(last_event);attroff(COLOR_PAIR(3));
        move(5,30);
        printEvent(last_event1);
        move(6,30);
        printEvent(last_event2);

        //Drone pose
        move(9,9);
        printStream(base_drone_pose.pose.pose.position.x);printw(" m  ");
        move(10,9);
        printStream(base_drone_pose.pose.pose.position.y);printw(" m  ");
        move(11,9);
        printStream(base_drone_pose.pose.pose.position.z);printw(" m  ");

        //Drone speed
        move(9,34);
        printStream(base_drone_pose.twist.twist.linear.x);printw(" m/s  ");
        move(10,34);
        printStream(base_drone_pose.twist.twist.linear.y);printw(" m/s  ");
        move(11,34);
        printStream(base_drone_pose.twist.twist.linear.z);printw(" m/s  ");

        //Drone orientation
        move(9,64);
        printStream(roll);printw(" rad  ");  
        move(10,64);
        printStream(pitch);printw(" rad  ");
        move(11,64);
        printStream(yaw);printw(" rad  ");

        //Far drone pose
        move(14,49);
        printStream(target_far_pose.pose.position.x);
        move(15,49);
        printStream(target_far_pose.pose.position.y);
        move(16,49);
        printStream(target_far_pose.pose.position.z);
        move(17,49);
        printStream(calculateDistance(target_far_pose));printw(" m  ");  

        //Close drone pose
        move(14,69);
        printStream(target_close_pose.pose.position.x);
        move(15,69);
        printStream(target_close_pose.pose.position.y);
        move(16,69);
        printStream(target_close_pose.pose.position.z);
        move(17,69);
        printStream(calculateDistance(target_close_pose));printw(" m  ");   

        //Counter consecutive detections
        move(15,13);
        printStreamInt(counter_far_consecutive_detections);
        move(16,13);
        printStreamInt(counter_close_consecutive_detections);
        move(17,13);
        printStreamInt(counter_close_consecutive_detections);

        //Last time seen detections    
        move(15,27);
        if (counter_far_time == 0) printTime(0);
        else printTime(ros::Time::now().toSec() - counter_far_time);
        move(16,27);
        if (counter_close_time == 0) printTime(0);
        else printTime(ros::Time::now().toSec() - counter_close_time);
        move(17,27);
        if (counter_ball_time == 0) printTime(0);
        else printTime(ros::Time::now().toSec() - counter_ball_time);

        //  Read input
        command = getch();
        switch (command){
            case '0':  // STOP & RESET
                move(23,0);
                clrtoeol(); refresh();
                if (command_confirm == -1){
                    move(23,7);
                    clrtoeol(); refresh();
                    attron(COLOR_PAIR(3));
                    printw("   Press 0 to confirm 'STOP & RESET' or other key to cancel command  ");
                    attroff(COLOR_PAIR(3));
                    command_confirm = 0;
                }else{
                    if(command_confirm == 0){
                        move(23,10);
                        clrtoeol(); refresh();
                        attron(COLOR_PAIR(3));
                        printw("             Last manual command: STOP & RESET                   ");
                        attroff(COLOR_PAIR(3));
                        // Execute STOP & RESET
                        mission_started = false;
                        msg_command.data = 0;  
                        command_pub.publish(msg_command);     
                    }else{
                        move(23,10);
                        clrtoeol(); refresh();
                        attron(COLOR_PAIR(2));
                        printw("                       Command canceled                          ");   
                        attroff(COLOR_PAIR(2));                 
                    }
                    command_confirm = -1;
                }
            break;
            case '1':  // MANUAL TAKE_OFF
                move(23,0);
                clrtoeol(); refresh();
                if (command_confirm == -1){
                    move(23,7);
                    clrtoeol(); refresh();
                    attron(COLOR_PAIR(3));
                    printw("    Press 1 to confirm 'TAKE_OFF' or other key to cancel command     ");
                    attroff(COLOR_PAIR(3));
                    command_confirm = 1;
                }else{
                    if(command_confirm == 1){
                        move(23,10);
                        clrtoeol(); refresh();
                        attron(COLOR_PAIR(3));
                        printw("                 Last manual command: TAKE_OFF                   ");
                        attroff(COLOR_PAIR(3));
                        // Execute TAKE_OFF
                        if(!mission_started){
                            mission_started = true;
                            mission_time = ros::Time::now().toSec();
                        }
                        msg_command.data = 1;  
                        command_pub.publish(msg_command);
                    }else{
                        move(23,10);
                        clrtoeol(); refresh();
                        attron(COLOR_PAIR(2));
                        printw("                       Command canceled                          ");
                        attroff(COLOR_PAIR(2));                   
                    }
                    command_confirm = -1;
                }
            break;
            case '2':  // MANUAL LAND
                move(23,0);
                clrtoeol(); refresh();
                if (command_confirm == -1){
                    move(23,7);
                    clrtoeol(); refresh();
                    attron(COLOR_PAIR(3));
                    printw("      Press 2 to confirm 'LAND' or other key to cancel command       ");
                    attroff(COLOR_PAIR(3));
                    command_confirm = 2;
                }else{
                    if(command_confirm == 2){
                        move(23,10);
                        clrtoeol(); refresh();
                        attron(COLOR_PAIR(3));
                        printw("                   Last manual command: LAND                     ");
                        attroff(COLOR_PAIR(3));
                        // Execute LAND  
                        msg_command.data = 2;  
                        command_pub.publish(msg_command);     
                    }else{
                        move(23,10);
                        clrtoeol(); refresh();
                        attron(COLOR_PAIR(2));
                        printw("                       Command canceled                          ");
                        attroff(COLOR_PAIR(2));                  
                    }
                    command_confirm = -1;
                }
            break;
            case '3':  // MANUAL NEXT_STATE
                move(23,0);
                clrtoeol(); refresh();
                if (command_confirm == -1){
                    move(23,7);
                    clrtoeol(); refresh();
                    attron(COLOR_PAIR(3));
                    printw("    Press 3 to confirm 'NEXT_STATE' or other key to cancel command   ");
                    attroff(COLOR_PAIR(3));
                    command_confirm = 3;
                }else{
                    if(command_confirm == 3){
                        move(23,10);
                        clrtoeol(); refresh();
                        attron(COLOR_PAIR(3));
                        printw("                Last manual command: NEXT_STATE                  ");
                        attroff(COLOR_PAIR(3));
                        // Execute NEXT_STATE 
                        msg_command.data = 3;  
                        command_pub.publish(msg_command); 
                    }else{
                        move(23,10);
                        clrtoeol(); refresh();
                        attron(COLOR_PAIR(2));
                        printw("                       Command canceled                          ");
                        attroff(COLOR_PAIR(2));               
                    }
                    command_confirm = -1;
                }
            break;
            case '4':  // MANUAL PREV_STATE
                move(23,0);
                clrtoeol(); refresh();
                if (command_confirm == -1){
                    move(23,7);
                    clrtoeol(); refresh();
                    attron(COLOR_PAIR(3));
                    printw("  Press 4 to confirm 'PREVIOUS_STATE' or other key to cancel command ");
                    attroff(COLOR_PAIR(3));
                    command_confirm = 4;
                }else{
                    if(command_confirm == 4){
                        move(23,10);
                        clrtoeol(); refresh();
                        attron(COLOR_PAIR(3));
                        printw("             Last manual command: PREVIOUS_STATE                 ");
                        attroff(COLOR_PAIR(3));
                        // Execute PREVIOUS STATE
                        msg_command.data = 4;  
                        command_pub.publish(msg_command);         
                    }else{
                        move(23,10);
                        clrtoeol(); refresh();
                        attron(COLOR_PAIR(2));
                        printw("                       Command canceled                          ");
                        attroff(COLOR_PAIR(2));                   
                    }
                    command_confirm = -1;
                }
            break;
        }
        //Refresh
        refresh();
        loop_rate.sleep();
    }

    //End ncurses
    endwin();
    return 0;
}

//Print float using stringstream
void printStream(float var) {
    interface_printout_stream.clear();
    interface_printout_stream.str(std::string());
    if (var > -0.01){
        interface_printout_stream << std::setw(5) << std::internal << fabs(var);
        attron(COLOR_PAIR(1));printw(" %s",interface_printout_stream.str().c_str());attroff(COLOR_PAIR(1));
    }else{
        interface_printout_stream << std::setw(6) << std::internal << var;
        attron(COLOR_PAIR(2));printw("%s",interface_printout_stream.str().c_str());attroff(COLOR_PAIR(2));
    }
} 

//Print float using stringstream
void printStreamInt(int var) {
    interface_printout_stream.clear();
    interface_printout_stream.str(std::string());
    interface_printout_stream << std::setw(2) << std::internal << var;
    printw(" %s",interface_printout_stream.str().c_str());
} 

//Print float using stringstream
void printDistance(float var) {
    interface_printout_stream.clear();
    interface_printout_stream.str(std::string());
    interface_printout_stream << std::setw(5) << std::internal << var;
    printw("%s",interface_printout_stream.str().c_str());
} 

//Print double using stringstream
void printStream(double var) {
    interface_printout_stream.clear();
    interface_printout_stream.str(std::string());
    if (var > -0.01){
        interface_printout_stream << std::setw(5) << std::internal << fabs(var);
        attron(COLOR_PAIR(1));printw(" %s",interface_printout_stream.str().c_str());attroff(COLOR_PAIR(1));
    }else{
        interface_printout_stream << std::setw(6) << std::internal << var;
        attron(COLOR_PAIR(2));printw("%s",interface_printout_stream.str().c_str());attroff(COLOR_PAIR(2));
    }
}

//Print state
void printState(int state) {
    switch(state){
        case (int)Item::State::START_STATE:
            printw("%s","START_STATE         ");
        break;
        case (int)Item::State::TAKE_OFF:
            printw("%s","TAKE_OFF            ");
        break;
        case (int)Item::State::SEARCH:
            printw("%s","SEARCHING           ");
        break;
        case (int)Item::State::FOLLOW_DRONE_CLOSE:
            printw("%s","FOLLOW_DRONE_CLOSE  ");
        break;
        case (int)Item::State::FOLLOW_DRONE_FAR:
            printw("%s","FOLLOW_DRONE_FAR    ");
        break;
        case (int)Item::State::RECOVER:
            printw("%s","RECOVER             ");
        break;
        case (int)Item::State::INTERCEPTION:
            printw("%s","INTERCEPTION        ");
        break;
        case (int)Item::State::CATCH_BALL:
            printw("%s","CATCH_BALL          ");
        break;
        case (int)Item::State::LAND:
            printw("%s","LAND                ");
        break;
        case (int)Item::State::FOCUS:
            printw("%s","FOCUS                ");
        break;
        default:
            printw("%s","    -               ");        
        break;
    }
}

//Print event
void printEvent(int event) {
    switch(event){
        case (int)Item::Event::TAKE_OFF:
            printw("%s","TAKE_OFF              ");
        break;
        case (int)Item::Event::TAKE_OFF_COMPLETED:
            printw("%s","TAKE_OFF_COMPLETED    ");
        break;
        case (int)Item::Event::FAR_DRONE_DETECTED:
            printw("%s","FAR_DRONE_DETECTED    ");
        break;
        case (int)Item::Event::CLOSE_DRONE_DETECTED:
            printw("%s","CLOSE_DRONE_DETECTED  ");
        break;
        case (int)Item::Event::FOLLOW_COMPLETED:
            printw("%s","FOLLOW_COMPLETED      ");
        break;
        case (int)Item::Event::DRONE_DETECTION_LOST:
            printw("%s","DRONE_DETECTION_LOST  ");
        break;
        case (int)Item::Event::BALL_DETECTED:
            printw("%s","CLOSE_BALL_DETECTED   ");
        break;
        case (int)Item::Event::BALL_DETECTION_LOST:
            printw("%s","BALL_DETECTION_LOST   ");
        break;
        case (int)Item::Event::SUCCESSFUL_CATCH:
            printw("%s","SUCCESSFUL_CATCH      ");
        break;
        case (int)Item::Event::FAILED_CATCH:
            printw("%s","FAILED_CATCH          ");
        break;
        case (int)Item::Event::ARRIVED_TO_SEARCH:
            printw("%s","ARRIVED_TO_SEARCH          ");
        break;
        case (int)Item::Event::TIMEOUT_RECOVER:
            printw("%s","TIMEOUT_RECOVER          ");
        break;
        default:
            printw("%s","    -               ");        
        break;
    }
}

void printStaticMenu(){
    move(0,32);
    printw("- MISSION VIEWER -");

    move(2,5);
    printw("States ");
    move(2,30);
    printw("Events    ");

    move(2,55);
    printw("Time remaining: ");
    move(3,55);
    printw("Battery:    ");

    //Drone pose
    move(8,5);
    printw("DRONE POSE");
    move(9,5);
    printw("x: ");
    move(10,5);
    printw("y: ");
    move(11,5);
    printw("z: ");

    //Drone speed
    move(8,30);
    printw("DRONE SPEED");
    move(9,30);
    printw("x: ");
    move(10,30);
    printw("y: ");
    move(11,30);
    printw("z: ");

    //Drone orientation
    move(8,55);
    printw("DRONE ORIENTATION");
    move(9,55);
    printw("roll: ");
    move(10,55);
    printw("pitch: ");
    move(11,55);
    printw("yaw: ");

    //Far drone pose
    move(13,42);
    printw("FAR DRONE POSE");
    move(14,42);
    printw("x: ");
    move(15,42);
    printw("y: ");
    move(16,42);
    printw("z: ");
    move(17,42);
    printw("Dist: ");

    //Close drone pose
    move(13,62);
    printw("CLOSE DRONE POSE");
    move(14,62);
    printw("x: ");
    move(15,62);
    printw("y: ");
    move(16,62);
    printw("z: ");
    move(17,62);
    printw("Dist: ");

    //Counter consecutive detections
    move(14,3);
    printw("COUNTER DETECTIONS");
    move(15,3);
    printw("Far:   ");
    move(16,3);
    printw("Close: ");
    move(17,3);
    printw("Ball: ");

    //Time consecutive detections
    move(14,22);
    printw("| LAST TIME SEEN");

    move(19,30);
    printw("- MISSION COMMANDS -");
    move(21,6);
    printw("0:STOP & RESET  1:TAKE_OFF  2:LAND  3:NEXT_STATE  4:PREVIOUS_STATE");
}

//Print battery charge
void printBattery(){
    float percentage = battery_msg.percentage*100;
    if(percentage == 100) {
        move(3,73);
        attron(COLOR_PAIR(1));printw("%.0f",percentage);attroff(COLOR_PAIR(1));
    }
    if(percentage > 50 && percentage < 100) {
        move(3,74);
        attron(COLOR_PAIR(1));printw("%.0f",percentage);attroff(COLOR_PAIR(1));
    }
    if(percentage <= 50 && percentage > 20) {
        move(3,74);
        attron(COLOR_PAIR(4));printw("%.0f",percentage);attroff(COLOR_PAIR(4));
    }
    if(percentage <= 20) {
        if (percentage > 10) move(3,24);
        else move(3,75);
        attron(COLOR_PAIR(2));printw("%.0f",percentage);attroff(COLOR_PAIR(2));     
    }
}

//Print distance between target pose and base drone pose
float calculateDistance(geometry_msgs::PoseStamped target){
    return sqrt(pow(base_drone_pose.pose.pose.position.x-target.pose.position.x,2)
        +pow(base_drone_pose.pose.pose.position.y-target.pose.position.y,2)
        +pow(base_drone_pose.pose.pose.position.z-target.pose.position.z,2));
}

void printTime(int seconds){
    interface_printout_stream.clear();
    interface_printout_stream.str(std::string());
    interface_printout_stream << std::setfill('0') << std::setw(2) << (seconds / 60) << ":"
    << std::setfill('0') << std::setw(2) << (seconds % 60);
    printw(" %s",interface_printout_stream.str().c_str());
}
