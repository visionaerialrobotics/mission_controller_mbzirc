#include "behavior_execution_balloon.h"

//Constructor
BehaviorExecutionBalloon::BehaviorExecutionBalloon(){
    balloon_pose_flag = false;
    base_link_flag = false;
    drone_far_flag = false;
    drone_close_flag = false;
    ball_close_flag = false;
    save_pose_take_off_flag = false;
    reached_first_burst_point_ = false;

    //current exploration waypoint
    current_traj_num_ = 0;
    trajectory_started_             = false;
    use_last_search_point_          = false;
    reducing_altitude_              = false;
    reaching_in_front_of_balloon_   = false;
    reached_last_burst_point_       = false;

    y_axis_offset_       = 0;
    front_x_axis_offset_ = 0;
    balloon_altitude_    = 0;
}

//Destructor
BehaviorExecutionBalloon::~BehaviorExecutionBalloon() {
}

void BehaviorExecutionBalloon::setYaxisOffset(double val){
    y_axis_offset_ = val;
 		std::cout << "y offset for burst: " << y_axis_offset_ << std::endl;
}

void BehaviorExecutionBalloon::setFrontXaxisOffset(double val){
    front_x_axis_offset_ = val;
  	std::cout << "x offset in front of balloon: " << front_x_axis_offset_ << std::endl;
   
}

void BehaviorExecutionBalloon::setBackXaxisoffset(double val){
		back_x_axis_offset_ = val;
		std::cout << "back x axis offset after burst: " << back_x_axis_offset_ << std::endl;
}

void BehaviorExecutionBalloon::setExplorationAltitude(double val){
    exploration_altitude_ = val;
		std::cout << "exploration altitude for drone: " << exploration_altitude_ << std::endl;
}


void BehaviorExecutionBalloon::setBalloonAltitude(double val){
    balloon_altitude_ = val;
  	std::cout << "balloon altitude for burst: " << balloon_altitude_ << std::endl;
}

void BehaviorExecutionBalloon::setExplorationYaw(double val){
		exploration_yaw_ = val;
		std::cout << "exploration yaw for burst: " << exploration_yaw_ << std::endl;
}

//Set poses
void BehaviorExecutionBalloon::setBallPose(geometry_msgs::PoseStamped ball_pose)
{
    if(!balloon_pose_flag){
        balloon_pose_flag = true;
    }
    this->ball_pose = ball_pose;
}

void BehaviorExecutionBalloon::setBaseLinkPose(geometry_msgs::PoseStamped base_link_pose)
{
    if(!base_link_flag){
        base_link_flag = true;
    }
    this->base_pose = base_link_pose;
}

//Set last time poses were checked
void BehaviorExecutionBalloon::setBallTime(double time_ball){
    this->time_ball = time_ball;
}

void BehaviorExecutionBalloon::getCurrentTrajectoryNum(int &num)
{
    num = current_traj_num_;
}

void BehaviorExecutionBalloon::setVectorOfExplorationPoints(std::vector<geometry_msgs::Point> exploration_points_vec)
{
    exploration_points_vec_ = exploration_points_vec;
}

void BehaviorExecutionBalloon::setCurrentExplorationPoint(geometry_msgs::PoseStamped pose)
{
    if(reached_last_burst_point_)
        current_exploration_point_ = pose;
    else
    {
        current_exploration_point_.pose.position.x =0;
        current_exploration_point_.pose.position.y =0;
        current_exploration_point_.pose.position.z =0;
    }
}

void BehaviorExecutionBalloon::getCurrentExplorationPoint(geometry_msgs::PoseStamped &pose,
                                                          bool& reached_burst_point)
{
    if(reached_last_burst_point_){
        reached_burst_point = reached_last_burst_point_;
        pose = current_exploration_point_;
    }
    else{
        reached_burst_point = reached_last_burst_point_;
        pose.pose.position.x = 0;
        pose.pose.position.y = 0;
        pose.pose.position.z = 0;
    }
}
//Returns:
// 0 data does not need to be sent
// 1 planner goal points need to be sent
// 2 external yaw need to be sent
// 3 planner goal and external yaw need to be sent
// 4 take off high level command needs to be sent
// 5 land high level command needs to be sent

int BehaviorExecutionBalloon::executeState(ItemBalloon::State state, std::vector<geometry_msgs::PoseStamped> & points_list,float & inflation_radius,float & gear_speed,nav_msgs::Odometry & external_yaw){
    int result = 0;
    switch(state){
    case ItemBalloon::State::TAKE_OFF:
        //std::cout << "[Executing] TAKE_OFF" << std::endl;
        if (behaviorTakeOff(points_list,inflation_radius,gear_speed)){
            result = 4;
        }else {
            result = 0;
        }
        break;
    case ItemBalloon::State::SEARCH_BALLOON:
        //std::cout << "[Executing] SEARCHING" << std::endl;
        reducing_altitude_ = false;
        reaching_in_front_of_balloon_ = false;
        reached_first_burst_point_    = false;
        reached_last_burst_point_     = false;
        if(behaviorSearchBalloon(points_list,inflation_radius,gear_speed)){ // Return true if a new trayectory is needed
            result = 3;
        }else{
            result = 2;
        }
        publishExternalYaw(state, external_yaw);

        break;
    case ItemBalloon::State::BURST_BALLOON:
        std::cout << "[Executing] BURST_BALLOON" << std::endl;

        if(!reduceAltitude(points_list, inflation_radius,gear_speed))
        {
            if(!reachInFrontOfBalloon(points_list, inflation_radius, gear_speed))
            {
                if(behaviorBurstBalloon(points_list,inflation_radius,gear_speed)){ // Return true if a new trayectory is needed
                    result = 3;
                }else{
                    result = 2;
                }
            }
            else{
                result = 3;
            }
        }
        else
	{
            result = 3;
        }
	//std::cout << "base pose " << base_pose.pose.position.x << "," << base_pose.pose.position.y << "," << base_pose.pose.position.z << std::endl;
	publishExternalYaw(state, external_yaw);

        break;
    case ItemBalloon::State::LAND:
        //std::cout << "[Executing] LAND" << std::endl;
        if(behaviorLand(points_list,inflation_radius,gear_speed)){
            result = 5;
        }else{
            result = 3;
        }
        break;
    }
    return result;
}

//Returns:
// 0 do nothing
// 1 enable far and close
// 2 disable and reset far and close
// 3 disable far and enable close
// 4 start search behavior

void BehaviorExecutionBalloon::resetFlags(ItemBalloon::State state){
    int result = 0;
    switch(state){
    case ItemBalloon::State::START_STATE:

        break;
    case ItemBalloon::State::TAKE_OFF:

        break;
    case ItemBalloon::State::SEARCH_BALLOON:

        break;

    case ItemBalloon::State::BURST_BALLOON:


        break;
    case ItemBalloon::State::LAND:

        break;
    }
}

bool BehaviorExecutionBalloon::behaviorSearchBalloon(std::vector<geometry_msgs::PoseStamped> & points_list,float & inflation_radius,float & gear_speed){

    double DISTANCE_THRESHOLD = 1;

    inflation_radius = -1.0;
    gear_speed       = 3.0;

    points_list.clear();

    if(current_traj_num_ < exploration_points_vec_.size()){
        if(!trajectory_started_){

            //send the first point
            geometry_msgs::PoseStamped point;
            point.pose.position.x = exploration_points_vec_[current_traj_num_].x;
            point.pose.position.y = exploration_points_vec_[current_traj_num_].y;
            point.pose.position.z = exploration_points_vec_[current_traj_num_].z;

            points_list.push_back(point);
            trajectory_started_ = true;

            std::cout << "sending first search point " << std::endl;
	    std::cout << "point: " << point.pose.position.x << "," << point.pose.position.y << "," << point.pose.position.z << std::endl;
            return true;
        }
        else if(trajectory_started_ && use_last_search_point_){

            //send the point before burst
            if(!increased_altitude_after_burst_)
            {
                geometry_msgs::PoseStamped altitude_point;
                altitude_point.pose.position.x = base_pose.pose.position.x;
                altitude_point.pose.position.y = base_pose.pose.position.y;
                altitude_point.pose.position.z = exploration_altitude_;

                if(distanceBetweenPoses(altitude_point, base_pose) < DISTANCE_THRESHOLD){
                    increased_altitude_after_burst_ = true;
                }
                else {
                    points_list.push_back(altitude_point);
                    return true;
                }

            }

            geometry_msgs::PoseStamped point;
            if(distanceBetweenPointAndPose(last_search_point_, base_pose) < DISTANCE_THRESHOLD){
                point.pose.position.x = exploration_points_vec_[current_traj_num_].x;
                point.pose.position.y = exploration_points_vec_[current_traj_num_].y;
                point.pose.position.z = exploration_points_vec_[current_traj_num_].z;
                points_list.push_back(point);
                use_last_search_point_ = false;
                return true;

            }
            else{

                point.pose.position.x = last_search_point_.x;
                point.pose.position.y = last_search_point_.y;
                point.pose.position.z = last_search_point_.z;
                points_list.push_back(point);

                std::cout << "sending the last saved traj point " << std::endl;
                std::cout << "last search point " << "X: " << point.pose.position.x << "," << "Y:" <<
                             point.pose.position.y << "," << "Z:" << point.pose.position.z << std::endl;
                return true;

            }

        }
        else if(trajectory_started_ && distanceBetweenPointAndPose(exploration_points_vec_[current_traj_num_], base_pose) < DISTANCE_THRESHOLD){
            //send the next point
            current_traj_num_++;
            if(current_traj_num_ < exploration_points_vec_.size())
            {
                geometry_msgs::PoseStamped point;
                point.pose.position.x = exploration_points_vec_[current_traj_num_].x;
                point.pose.position.y = exploration_points_vec_[current_traj_num_].y;
                point.pose.position.z = exploration_points_vec_[current_traj_num_].z;

                std::cout << "sending next point " << std::endl << "X:" << point.pose.position.x << "Y:" << point.pose.position.y << "Z: " << point.pose.position.z << std::endl;
                points_list.push_back(point);

                return true;
            }
            else
                return false;
        }
        else {
            return false;
        }
    }
    else {
        std::cout << "sent all the exploration points" << std::endl;
        return false;
    }

}

bool BehaviorExecutionBalloon::behaviorBurstBalloon(std::vector<geometry_msgs::PoseStamped> & points_list,float & inflation_radius,float & gear_speed)
{
    //if(balloon_pose_flag){
    //  return false;
    //}

    //if (time_ball != behaviorBurstBallon_lastTime){
    //    behaviorBurstBallon_lastTime = time_ball;

        if(!use_last_search_point_){

            last_search_point_.x = base_pose.pose.position.x;
            last_search_point_.y = base_pose.pose.position.y + y_axis_offset_;
            last_search_point_.z = exploration_altitude_;
            use_last_search_point_ = true;
            increased_altitude_after_burst_ = false;

        }
        std::cout << "ball pose " << ball_pose.pose.position.x << "," << ball_pose.pose.position.y << "," << ball_pose.pose.position.z << std::endl;

        points_list.clear();
        if(!reached_first_burst_point_){
            geometry_msgs::PoseStamped first_point;
            first_point.pose.position.x = ball_pose.pose.position.x;
            first_point.pose.position.y = ball_pose.pose.position.y + y_axis_offset_;
            first_point.pose.position.z = balloon_altitude_;
            points_list.push_back(first_point);
						
	    std::cout << "first_point " << "X:" << first_point.pose.position.x << "Y:" << first_point.pose.position.y << "Z:" << first_point.pose.position.z << std::endl;

            if(distanceBetweenPoses(first_point, base_pose) < 0.5)
                reached_first_burst_point_ = true;
        }
        else{
            geometry_msgs::PoseStamped second_point;
            second_point.pose.position.x = ball_pose.pose.position.x + cos(exploration_yaw_) * back_x_axis_offset_;
            second_point.pose.position.y = ball_pose.pose.position.y + y_axis_offset_;
            second_point.pose.position.z = balloon_altitude_;
	    std::cout << "second_point " << "X:" << second_point.pose.position.x << "Y:" << second_point.pose.position.y << "Z:" << second_point.pose.position.z << std::endl;

            points_list.push_back(second_point);
            reached_last_burst_point_ = true;
            this->setCurrentExplorationPoint(points_list[0]);
        }

        inflation_radius = -1;
        gear_speed = 3.0;

        return true;

    //}else{
    //    return false;

    //}
}

bool BehaviorExecutionBalloon::reduceAltitude(std::vector<geometry_msgs::PoseStamped>& points_list, float& inflation_radius, float& gear_speed)
{
    points_list.clear();
    geometry_msgs::PoseStamped altitude_point;
    altitude_point.pose.position.x = base_pose.pose.position.x;
    altitude_point.pose.position.y = base_pose.pose.position.y;
    altitude_point.pose.position.z = exploration_altitude_;

    inflation_radius = -1;
    gear_speed = 3;

    if(!reducing_altitude_ && distanceBetweenPoses(altitude_point, base_pose) > 0.5){
        points_list.push_back(altitude_point);
        std::cout << "reducing altitude" << std::endl;
	std::cout << "altitude point " << altitude_point.pose.position.x << ", " << altitude_point.pose.position.y << "," << altitude_point.pose.position.z << std::endl;
        return true;
    }
    else
    {
        std::cout << "reduced altitude" << std::endl;
        reducing_altitude_ = true;
        return false;
    }
}

bool BehaviorExecutionBalloon::reachInFrontOfBalloon(std::vector<geometry_msgs::PoseStamped> &points_list, float &inflation_radius, float &gear_speed)
{
    points_list.clear();
    geometry_msgs::PoseStamped front_point;
    front_point.pose.position.x = ball_pose.pose.position.x - cos(exploration_yaw_) * front_x_axis_offset_;
    front_point.pose.position.y = ball_pose.pose.position.y;
    front_point.pose.position.z = exploration_altitude_;

    inflation_radius = -1;
    gear_speed = 3;

    if(!reaching_in_front_of_balloon_ && distanceBetweenPoses(front_point, base_pose) > 0.5){
        points_list.push_back(front_point);
			  std::cout << "front balloon point " << front_point.pose.position.x << "," << front_point.pose.position.y << "," << front_point.pose.position.z << std::endl;
        std::cout << "reaching in front of the balloon" << std::endl;
        return true;
    }
    else
    {
        std::cout << "reached in front of the balloon" << std::endl;
        reaching_in_front_of_balloon_ = true;
        return false;
    }
}

//Calculates distance between two poses
double BehaviorExecutionBalloon::distanceBetweenPoses(geometry_msgs::PoseStamped pose_1, geometry_msgs::PoseStamped pose_2){
    return sqrt(pow(pose_1.pose.position.x - pose_2.pose.position.x,2)+
                pow(pose_1.pose.position.y - pose_2.pose.position.y,2)+
                pow(pose_1.pose.position.z - pose_2.pose.position.z,2));
}

double BehaviorExecutionBalloon::distanceBetweenPointAndPose(geometry_msgs::Point point_1, geometry_msgs::PoseStamped pose_2){
    return sqrt(pow(point_1.x - pose_2.pose.position.x,2)+
                pow(point_1.y - pose_2.pose.position.y,2)+
                pow(point_1.z - pose_2.pose.position.z,2));
}

bool BehaviorExecutionBalloon::behaviorTakeOff(std::vector<geometry_msgs::PoseStamped> & points_list,float & inflation_radius,float & gear_speed){

    if(!save_pose_take_off_flag){
        save_pose_take_off_flag = true;
        this->take_off_pose_.pose.position.x = base_pose.pose.position.x;
        this->take_off_pose_.pose.position.y = base_pose.pose.position.y;
        this->take_off_pose_.pose.position.z = exploration_altitude_;

        std::cout << "Position take off: " << base_pose.pose.position.x << ", " << base_pose.pose.position.y << ", "<< base_pose.pose.position.z << std::endl;

        points_list.clear();

        geometry_msgs::PoseStamped first_point;
        first_point.pose.position.x = base_pose.pose.position.x;
        first_point.pose.position.y = base_pose.pose.position.y;
        first_point.pose.position.z = exploration_altitude_;
        points_list.push_back(first_point);

        inflation_radius = -1.0;
        gear_speed = 1.0;
        return true;

    }else{
        return false;
    }



    // 2º Go to the initial altitude
}

bool BehaviorExecutionBalloon::behaviorLand(std::vector<geometry_msgs::PoseStamped> & points_list,float & inflation_radius,float & gear_speed){
    double THRESHOLD_LANDING = 0.5;
    points_list.clear();

    if(distanceBetweenPoses(base_pose,take_off_pose_) <  THRESHOLD_LANDING){
        std::cout << "Landing DJI " << std::endl;
        return true;
    }else{
        //std::cout << "Position take off: " << take_off_pose_.pose.position.x << ", " << take_off_pose_.pose.position.y << ", "<< take_off_pose_.pose.position.z << std::endl;
        //std::cout << "Position take of is : " << distanceBetweenPoses(base_pose,take_off_pose_) << " meters far. Threshold is : " << THRESHOLD_LANDING << std::endl;

        std::cout << "reaching init point for landing " << std::endl;
        geometry_msgs::PoseStamped first_point;
        first_point.pose.position.x = take_off_pose_.pose.position.x;
        first_point.pose.position.y = take_off_pose_.pose.position.y;
        first_point.pose.position.z = take_off_pose_.pose.position.z;
        points_list.push_back(first_point);

        inflation_radius = -1.0;
        gear_speed = 1.0;

        return false;
    }


}

//Publish yaw
void BehaviorExecutionBalloon::publishExternalYaw(ItemBalloon::State state, nav_msgs::Odometry & external_yaw){
      external_yaw.pose.pose.orientation.z = exploration_yaw_;
}
