#ifndef BEHAVIOR_EXECUTION_H
#define BEHAVIOR_EXECUTION_H

#include <iostream>
#include "state_machine.h"
#include <math.h>
#include "eigen3/Eigen/Eigen"
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Vector3Stamped.h>

#define ADJUST_ALTTITUDE
#define DONKEY_STATEGY
#define FRONT_DONKEY_STATEGY

#define CATCH_BALL_CLOSE_NO_KALMAN
#define DYNAMIC_OFFSET_CATCH

#define X_AXIS_SEARCH
#define SEARCH_VARIABLE_ALTITUDE 

class BehaviorExecution
{

public:
	void setUp();

	void start();

	void stop();

	//Constructor.
	BehaviorExecution();
	Item::State current_state;

	// Destructor.
	~BehaviorExecution();

        void setBallPose(geometry_msgs::PoseStamped ball_pose);
        void setBaseLinkPose(geometry_msgs::PoseStamped base_link_pose);
        void setDroneFarPose(geometry_msgs::PoseStamped drone_far_pose);
        void setDroneClosePose(geometry_msgs::PoseStamped drone_close_pose);
        void setDroneFuturePose(geometry_msgs::PoseStamped drone_future_pose);
        void setBallClosePose(geometry_msgs::PoseStamped ball_close_pose);
        void setBallClosePose_NO_kalman(geometry_msgs::PoseStamped ball_close_pose_NO_kalman);
        void setBallTime(double time_ball);
        void setDroneFarTime(double time_target_drone_far);
        void setDroneCloseTime(double time_target_drone_close);

        void setExternalGimbalAltitude(double target_angle,double min_altitude);
        void setGimbalAngle(geometry_msgs::Vector3Stamped gimbal_state);

	void setGripperOffset(Eigen::Vector3d gripperOffset);

        int executeState(Item::State state, std::vector<geometry_msgs::PoseStamped> & points_list,float & inflation_radius,float & gear_speed,nav_msgs::Odometry & external_yaw);
        double distanceBetweenPoses(geometry_msgs::PoseStamped pose_1, geometry_msgs::PoseStamped pose_2);
        double distanceBetweenPosesXY(geometry_msgs::PoseStamped pose_1, geometry_msgs::PoseStamped pose_2);

        void publishExternalYaw(Item::State state, nav_msgs::Odometry & external_yaw );
        bool behaviorTakeOff(std::vector<geometry_msgs::PoseStamped> & points_list,float & inflation_radius,float & gear_speed);
        bool behaviorLand(std::vector<geometry_msgs::PoseStamped> & points_list,float & inflation_radius,float & gear_speed);
        int resetFlags(Item::State state, bool & far_counter,  bool & closecounter, bool & ball_counter);
        bool behaviorRecover(std::vector<geometry_msgs::PoseStamped> & points_list,float & inflation_radius,float & gear_speed,nav_msgs::Odometry  external_yaw);
        void setCatchCounter(int counter_catch);

        void activateBallDetectionAllTime(bool activate_ball_detector_all_time);

        geometry_msgs::PoseStamped center_search_behavior;
        geometry_msgs::PoseStamped center_search_behaviour_offset_yaw;

        double width_box_donkey_strategy;
        double height_box_donkey_strategy;
        double width_arena_donkey_strategy;
        double height_arena_donkey_strategy;


        double h1_slow_DR; double h2_slow_DR; double d1_slow_DR; double d2_slow_DR;
        double h1_fast_DR; double h2_fast_DR; double d1_fast_DR; double d2_fast_DR;

        double gear_speed_far_DR;
        double gear_speed_focus_DR;
        double gear_speed_recover_DR;
        double gear_speed_search_DR;
        double gear_speed_takeoff_and_land_DR;

        double close_follow_distance_XY_drone_DR;
        double close_follow_distance_Z_drone_DR;

        double close_follow_distance_XY_ball_DR;
        double close_follow_distance_Z_ball_DR;

        double inflation_radius_follow_close_drone_DR;

        double gear_speed_catch_ball_DR;
        double distance_behind_ball_catch_DR;
        double offset_ball_catch_DR;
        double inflation_radius_ball_follow_DR;

	bool stop_increasing_altitude_search;
	int counter_increasing_altitude_search;

	double ALTITUDE_SEARCH;
        int TENT_STRATEGY;

        double OFFSET_TENT_STRATEGY_HEIGHT;
        double OFFSET_TENT_STRATEGY_WIDTH;

private:

    bool ACTIVATE_BALL_DETECTOR_ALL_TIME;

    double TARGET_ANGLE;
    double MIN_ALTITUDE;

    double previuos_distance_drone_close;
    double previuos_distance_ball;

    double behaviorFollowDroneClose_lastTime;
    double behaviorFollowBallClose_lastTime;
    double behaviorCatchBallClose_lastTime;

    bool gimbal_angles_flag;
    bool ball_pose_flag;
    bool base_link_flag;
    bool drone_far_flag;
    bool drone_close_flag;
    bool drone_future_flag;
    bool ball_close_flag;
    bool ball_close_flag_NO_kalman;
    bool save_pose_take_off_flag;
    bool reset_perception_counters_search;
    bool recover_first_time_call_flag;
    bool focus_descending_flag;
    bool focus_trajectory_in_progress_flag;

    bool gripper_is_close;

    bool recover_turning_around_in_progress_flag;

    bool far_perception_enable_flag;
    bool close_perception_enable_flag;
    double dynamic_offset_catch_z;




    //Search trajetory flags
    bool search_trajectory_in_progress_flag;
    bool search_trajectory_clockwise_flag;

    std::vector<geometry_msgs::PoseStamped> last_points_list_search;

    double last_external_yaw;
    std::vector<double> gimbal_angles_;

    geometry_msgs::PoseStamped take_off_pose_;
    std::vector<double> land_pose_;

    geometry_msgs::PoseStamped ball_pose;
    geometry_msgs::PoseStamped base_pose;
    geometry_msgs::PoseStamped drone_far_pose;
    geometry_msgs::PoseStamped drone_close_pose;
    geometry_msgs::PoseStamped drone_future_pose;
    geometry_msgs::PoseStamped ball_close_pose;
    geometry_msgs::PoseStamped ball_close_pose_NO_kalman;


    geometry_msgs::PoseStamped focus_end_point;

    bool end_focus_state;
    bool focus_achieved_goal_altitude;


    Eigen::Vector3d gripperOffset;

    double time_target_drone_far;
    double time_target_drone_close;
    double time_ball;

    double calculateGearSpeed(double previous_distance, double actual_distance);

    bool behaviorFocus(std::vector<geometry_msgs::PoseStamped> & points_list,float & inflation_radius,float & gear_speed);
    bool behaviorSearchDrone(std::vector<geometry_msgs::PoseStamped> & points_list,float & inflation_radius,float & gear_speed);
    void behaviorFollowDroneFar(std::vector<geometry_msgs::PoseStamped> & points_list,float & inflation_radius,float & gear_speed);
    bool behaviorFollowDroneClose(std::vector<geometry_msgs::PoseStamped> & points_list,float & inflation_radius,float & gear_speed);
    bool behaviorFollowBall(std::vector<geometry_msgs::PoseStamped> & points_list,float & inflation_radius,float & gear_speed);
    void behaviorInterceptDrone(std::vector<geometry_msgs::PoseStamped> & points_list,float & inflation_radius,float & gear_speed);
    int behaviorCatchBall(std::vector<geometry_msgs::PoseStamped> & points_list,float & inflation_radius,float & gear_speed);

    void calculateExternalYaw(double far);
    void safeDistanceWithNetSaturation(geometry_msgs::PoseStamped & point);
    void donkeyStrategySaturation(geometry_msgs::PoseStamped & point, int mode);
    bool getTrajectorySearch(std::vector<geometry_msgs::PoseStamped> & points_list);
    void adjustAltitudeWithPerception(geometry_msgs::PoseStamped & point);

    void resetFocusFlags();


};
#endif 
