#ifndef MISSION_VISUALIZER_BALLOON_ROSMODULE_H
#define MISSION_VISUALIZER_BALLOON_ROSMODULE_H

//STD CONSOLE
#include <sstream>
#include <stdio.h>
#include <curses.h>
#include <iostream>
#include <string>
#include <math.h> 
#include <std_msgs/Int32.h>
#include <std_msgs/Float32.h>
//ROS
#include "ros/ros.h"

#include "geometry_msgs/PoseStamped.h"
#include "communication_definition.h"
#include "sensor_msgs/BatteryState.h"
#include <nav_msgs/Odometry.h>

#include "balloon/mission_controller_balloon_mbzirc_process.h"
#include <tf2/LinearMath/Quaternion.h>
#include <tf2/LinearMath/Matrix3x3.h>

//Msgs
geometry_msgs::PoseStamped target_close_pose;
geometry_msgs::PoseStamped target_far_pose;
nav_msgs::Odometry base_drone_pose;
sensor_msgs::BatteryState battery_msg;

int counter_far_consecutive_detections;
int counter_far_time;
int counter_close_consecutive_detections;
int counter_close_time;
int counter_ball_consecutive_detections;
int counter_ball_time;

int current_state;
int past_state1;
int past_state2;
int past_state3;
int last_event;
int last_event1;
int last_event2;


//Variables
std::stringstream interface_printout_stream;
std::stringstream pinterface_printout_stream;
std::string drone_id_namespace;
double frequency;
int mission_time_seconds;
double roll, pitch, yaw;
int mission_time;
bool mission_started;

//Subscribers
ros::Subscriber target_far_pose_sub;
ros::Subscriber target_close_pose_sub;
ros::Subscriber drone_pose_sub;
ros::Subscriber counter_target_far_sub;
ros::Subscriber counter_target_close_sub;
ros::Subscriber counter_ball_sub;
ros::Subscriber current_state_sub;
ros::Subscriber last_event_sub;
ros::Subscriber battery_sub;
//Publishers
ros::Publisher command_pub;

std_msgs::Int32 msg_command;

//Print-Stream Functions
void printStream(float var);
void printStream(double var);
void printStreamInt(int var);
void printDistance(float var);
void printStaticMenu();
void printState(int state);
void printEvent(int event);
void printBattery();
void printTime(int seconds);

float calculateDistance(geometry_msgs::PoseStamped target);


//Callbacks
void targetCloseDronePoseCallback(const geometry_msgs::PoseStamped::ConstPtr &msg);
void targetFarDronePoseCallback(const geometry_msgs::PoseStamped::ConstPtr &msg);
void baseDronePoseCallback(const geometry_msgs::PoseStamped::ConstPtr &msg);
void counterTargetFarCallback(const std_msgs::Int32::ConstPtr &msg);
void counterTargetCloseCallback(const std_msgs::Int32::ConstPtr &msg);
void counterBallCallback(const std_msgs::Int32::ConstPtr &msg);
void batteryCallback(const sensor_msgs::BatteryState::ConstPtr& msg);
void eventCallback(const std_msgs::Int32::ConstPtr &msg);
void stateCallback(const std_msgs::Int32::ConstPtr &msg);
void odometryCallback(const nav_msgs::OdometryConstPtr &msg);


#endif 
