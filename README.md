# Mission Controller MBZIRC

This package contains files related to the mission for the MBZIRC competition. These files implement an interface (mission visualizer) and two state machines.

## Mission Visualizer MBZIRC

The keyboard interaction with this interface is explained below:

- **SPACEBAR:** The key SPACEBAR refreshes the terminal window and reprints the screen, it is useful after resizing the screen and allows everything to be printed correctly.
- **0:** The key 0 (STOP & RESET) stops the mission and resets the time counter.
- **1:** The key 1 (TAKE OFF) executes the take off. If it is the first take off of the mission, it will start the time counter.
- **2:** The key 2 (LAND) executes a landing.
- **3:** The key 3 (NEXT STATE) forces the state machine to move to the next state. 
- **4:** The key 4 (PREVIOUS STATE) forces the state machine to move to the previous state. 

# Contributors

**Code maintainer:** Adrián Álvarez

**Authors:** Alberto Rodelgo, Adrián Álvarez
