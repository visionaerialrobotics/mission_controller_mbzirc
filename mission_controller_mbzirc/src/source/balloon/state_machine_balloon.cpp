#include "state_machine_balloon.h"

//Constructor
StateMachineBalloon::StateMachineBalloon(){

  ACTIVATE_BALL_DETECTOR_ALL_TIME = false;
}

//Destructor
StateMachineBalloon::~StateMachineBalloon() {}

//Returns next state given the current_state and an event
void StateMachineBalloon::nextState(ItemBalloon::State & current_state, ItemBalloon::Event event)
{
  switch(current_state){

  // ************* START_STATE *************
  case ItemBalloon::State::START_STATE:
    if (event == ItemBalloon::Event::TAKE_OFF){
      current_state = ItemBalloon::State::TAKE_OFF;
      printState(current_state);
    }
    break;
    // ************* TAKE_OFF *************

  case ItemBalloon::State::TAKE_OFF:
    if (event == ItemBalloon::Event::TAKE_OFF_COMPLETED){
      current_state = ItemBalloon::State::SEARCH_BALLOON;
      printState(current_state);
    }
    break;

    // ************* SEARCH BALLOON *************

  case ItemBalloon::State::SEARCH_BALLOON:
    if(ACTIVATE_BALL_DETECTOR_ALL_TIME){
      if (event == ItemBalloon::Event::BALLOON_DETECTED){
        current_state = ItemBalloon::State::BURST_BALLOON;
        printState(current_state);
      }
      if (event == ItemBalloon::Event::SEARCH_COMPLETE){
        current_state = ItemBalloon::State::LAND;
        printState(current_state);
      }
    }

    break;

    // ************* BURST BALLOON *************
  case ItemBalloon::State::BURST_BALLOON:
    if (event == ItemBalloon::Event::SUCCESSFUL_BURST){
      current_state = ItemBalloon::State::SEARCH_BALLOON;
      printState(current_state);
    }
    if (event == ItemBalloon::Event::FAILED_BURST){
      current_state = ItemBalloon::State::SEARCH_BALLOON;
      printState(current_state);
    }
    break;

    // ************* LAND *************

  case ItemBalloon::State::LAND:
    if (event == ItemBalloon::Event::TAKE_OFF){
      current_state = ItemBalloon::State::TAKE_OFF;
      printState(current_state);
    }
    break;
  }
}


void StateMachineBalloon::printState(ItemBalloon::State state){
  switch(state){
  case ItemBalloon::State::START_STATE:
    std::cout << "\033[1;92m[State] START_STATE\033[0m" << std::endl;
    break;
  case ItemBalloon::State::TAKE_OFF:
    std::cout << "\033[1;92m[State] TAKE_OFF\033[0m" << std::endl;
    break;
  case ItemBalloon::State::SEARCH_BALLOON:
    std::cout << "\033[1;92m[State] SEARCHING BALLOON\033[0m" << std::endl;
    break;
  case ItemBalloon::State::BURST_BALLOON:
    std::cout << "\033[1;92m[State] BURST BALLOON\033[0m" << std::endl;
    break;
  case ItemBalloon::State::LAND:
    std::cout << "\033[1;92m[State] LAND\033[0m" << std::endl;
    break;
  }
}

void StateMachineBalloon::activateBallDetectionAllTime(bool activate_ball_detector_all_time){

  ACTIVATE_BALL_DETECTOR_ALL_TIME = activate_ball_detector_all_time;
}
