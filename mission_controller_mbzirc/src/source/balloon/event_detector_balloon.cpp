#include "event_detector_balloon.h"

//Constructor
EventDetectorBalloon::EventDetectorBalloon(){

    last_event = ItemBalloon::Event::UNKNOWN;

    counter_ball_close = 0;
    time_ball = 0;
    gripperOffset(0) = 0;
    gripperOffset(1) = 0;
    gripperOffset(2) = 0;
    ACTIVATE_BALL_DETECTOR_ALL_TIME = false;
    exploration_points_size_ = 0;
    current_traj_num_ = 0;

    reached_last_burst_point_ = false;
    wait_key_take_off = false;
}

//Destructor
EventDetectorBalloon::~EventDetectorBalloon() {}


//Set poses
void EventDetectorBalloon::setPoses(geometry_msgs::PoseStamped ball_pose,
                                    geometry_msgs::PoseStamped base_pose )
{
    this->ball_pose = ball_pose;
    this->base_pose = base_pose;
}

//Set last time poses were checkedBALLOON_DETECTED
void EventDetectorBalloon::setTime(double time_ball){
    this->time_ball = time_ball;
}

//Set gripperOffset
void EventDetectorBalloon::setGripperOffset(Eigen::Vector3d gripperOffset){
    this->gripperOffset = gripperOffset;
}


//Set counter consecutive ball detections with close detector
void EventDetectorBalloon::setCounterCloseBallDetector(int counter){
    this->counter_ball_close = counter;
}

void EventDetectorBalloon::setExplorationPointsSize(int num){
    exploration_points_size_ = num;
}

void EventDetectorBalloon::setCurrentTrajNum(int num){
    current_traj_num_ = num;
}

void EventDetectorBalloon::setCurrentExplorationPoint(geometry_msgs::PoseStamped pose, bool reached_last_burst_pose)
{
    reached_last_burst_point_ = reached_last_burst_pose;
    current_exploration_point_ = pose;
}

//Generate event based on TF and current state
bool EventDetectorBalloon::generateEvent(ItemBalloon::State current_state, ItemBalloon::Event & next_event){

    // return true whem there is an event
    switch(current_state){
    std::cout << " EVENT GENERATOR" << std::endl;

    // *********** State::START_STATE ****************
    case ItemBalloon::State::START_STATE:
        if(wait_key_take_off){
            next_event = ItemBalloon::Event::TAKE_OFF;
            return true;
        }else {
            return false;
        }
        break;

        // *********** State::TAKE_OFF ****************
    case ItemBalloon::State::TAKE_OFF:
        double TOLERANCE_ERROR;
        TOLERANCE_ERROR = 1;
	if(this->base_pose.pose.position.z + TOLERANCE_ERROR > 1.5){
            next_event = ItemBalloon::Event::TAKE_OFF_COMPLETED;
            return true;
        }else{
            return false;
        }
        break;

        // *********** State::SEARCHING ****************
    case ItemBalloon::State::SEARCH_BALLOON:
        //std::cout << " SEARCH" << std::endl;
        reached_last_burst_point_ = false;
        if(current_traj_num_ < exploration_points_size_){
            if(counter_ball_close >= 10){
                next_event = ItemBalloon::Event::BALLOON_DETECTED;
            }else{
                return false;
            }
        }
        else {
            next_event = ItemBalloon::Event::SEARCH_COMPLETE;
        }
        break;

        // ********** State::BURST_BALL *****************
    case ItemBalloon::State::BURST_BALLOON:

        //std::cout << "reached_last_burst_point in burst " << reached_last_burst_point_ << std::endl;
	//			std::cout << "current exploration point " << current_exploration_point_.pose.position.x << ", " << current_exploration_point_.pose.position.y
        //                  << " , " << current_exploration_point_.pose.position.z << std::endl;
	//		  std::cout << "base_pose " << base_pose.pose.position.x << ", " << base_pose.pose.position.y
        //                  << " , " << base_pose.pose.position.z << std::endl;

        if(reached_last_burst_point_){
            if(this->distanceBetweenPoses(base_pose, current_exploration_point_) < 0.5){
                next_event = ItemBalloon::Event::SUCCESSFUL_BURST;
            }
            else {
                return false;
            }
        }
        else{
            return false;
        }
        break;

        // ********** ItemBalloon::State::LAND *****************
    case ItemBalloon::State::LAND:

        return false;
        break;

    default:
        return false;
        break;
    }
    last_event = next_event;
    printEvent(next_event);
    return true;
}

void EventDetectorBalloon::printEvent(ItemBalloon::Event event){
    switch(event){
    case ItemBalloon::Event::TAKE_OFF:
        std::cout << "\033[1;94m[Event] TAKE_OFF\033[0m" << std::endl;
        break;
    case ItemBalloon::Event::TAKE_OFF_COMPLETED:
        std::cout << "\033[1;94m[Event] TAKE_OFF_COMPLETED\033[0m" << std::endl;
        break;
    case ItemBalloon::Event::BALLOON_DETECTED:
        std::cout << "\033[1;94m[Event] BALLOON_DETECTED\033[0m" << std::endl;
        break;
    case ItemBalloon::Event::SUCCESSFUL_BURST:
        std::cout << "\033[1;94m[Event] SUCCESSFUL_BURST\033[0m" << std::endl;
        break;
    case ItemBalloon::Event::FAILED_BURST:
        std::cout << "\033[1;94m[Event] FAILED_BURST\033[0m" << std::endl;
        break;
    default:
        std::cout << "Unkown event" << std::endl;

        break;
    }
}

void EventDetectorBalloon::activateBallDetectionAllTime(bool activate_ball_detector_all_time){

    ACTIVATE_BALL_DETECTOR_ALL_TIME = activate_ball_detector_all_time;
}

//calculates distance between two poses
double EventDetectorBalloon::distanceBetweenPoses(geometry_msgs::PoseStamped pose_1, geometry_msgs::PoseStamped pose_2){
    return sqrt(pow(pose_1.pose.position.x - pose_2.pose.position.x,2)+
                pow(pose_1.pose.position.y - pose_2.pose.position.y,2));
}
