#ifndef BEHAVIOR_EXECUTION_H
#define BEHAVIOR_EXECUTION_H

#include <iostream>
#include <fstream>
#include "state_machine_balloon.h"
#include <math.h>
#include "eigen3/Eigen/Eigen"
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Vector3Stamped.h>
#include <geometry_msgs/Point.h>

class BehaviorExecutionBalloon
{

public:
  void setUp();

  void start();

  void stop();

  //Constructor.
  BehaviorExecutionBalloon();
  ItemBalloon::State current_state;

  // Destructor.
  ~BehaviorExecutionBalloon();

  void setYaxisOffset(double val);
  void setFrontXaxisOffset(double val);
  void setBalloonAltitude(double val);
  void setExplorationAltitude(double val);
	void setExplorationYaw(double val);	
  void setBackXaxisoffset(double val);
  void setBallPose(geometry_msgs::PoseStamped ball_pose);
  void setBallPoseInBaseLink(geometry_msgs::PoseStamped pose);
  void setBaseLinkPose(geometry_msgs::PoseStamped base_link_pose);
  void setBallClosePose(geometry_msgs::PoseStamped ball_close_pose);

  void setBallTime(double time_ball);
  int executeState(ItemBalloon::State state, std::vector<geometry_msgs::PoseStamped> & points_list,float & inflation_radius,float & gear_speed,nav_msgs::Odometry & external_yaw);
  double distanceBetweenPoses(geometry_msgs::PoseStamped pose_1, geometry_msgs::PoseStamped pose_2);
  double distanceBetweenPointAndPose(geometry_msgs::Point point_1, geometry_msgs::PoseStamped pose_2);

  void publishExternalYaw(ItemBalloon::State state, nav_msgs::Odometry & external_yaw);
  bool behaviorTakeOff(std::vector<geometry_msgs::PoseStamped> & points_list,float & inflation_radius,float & gear_speed);
  bool behaviorLand(std::vector<geometry_msgs::PoseStamped> & points_list,float & inflation_radius,float & gear_speed);
  void resetFlags(ItemBalloon::State state);
  void setVectorOfExplorationPoints(std::vector<geometry_msgs::Point> exploration_points_vec);
  void activateBallDetectionAllTime(bool activate_ball_detector_all_time);
  void setCurrentExplorationPoint(geometry_msgs::PoseStamped pose);
  void getCurrentExplorationPoint(geometry_msgs::PoseStamped& pose, bool &reached_burst_point);

  void getCurrentTrajectoryNum(int &num);
  geometry_msgs::PoseStamped current_exploration_point_;

private:
  double y_axis_offset_;
  double front_x_axis_offset_;
  double back_x_axis_offset_;
  double balloon_altitude_;
  double exploration_altitude_;
  double exploration_yaw_;	

  bool reached_last_burst_point_;
  bool ACTIVATE_BALL_DETECTOR_ALL_TIME;
  bool balloon_pose_flag;
  bool base_link_flag;
  bool drone_far_flag;
  bool drone_close_flag;
  bool drone_future_flag;
  bool ball_close_flag;
  bool save_pose_take_off_flag;
  bool reset_perception_counters_search;
  bool reached_first_burst_point_;
	
  std::vector<geometry_msgs::PoseStamped> last_points_list_search;

  geometry_msgs::PoseStamped take_off_pose_;
  std::vector<double> land_pose_;

  geometry_msgs::PoseStamped ball_pose;
  geometry_msgs::PoseStamped base_pose;
  geometry_msgs::PoseStamped ball_pose_base_link;

  std::vector<geometry_msgs::Point> exploration_points_vec_;
  geometry_msgs::Point last_search_point_;
  int current_traj_num_;
  bool trajectory_started_;
  bool use_last_search_point_;

  bool end_focus_state;
  bool focus_achieved_goal_altitude;
  bool reducing_altitude_;
  bool reaching_in_front_of_balloon_;
  bool increased_altitude_after_burst_;

  Eigen::Vector3d gripperOffset;

  double time_target_drone_far;
  double time_target_drone_close;
  double time_ball;
  double behaviorBurstBallon_lastTime;	
  
  bool behaviorSearchBalloon(std::vector<geometry_msgs::PoseStamped> & points_list,float & inflation_radius,float & gear_speed);
  bool reduceAltitude(std::vector<geometry_msgs::PoseStamped>& points_list, float &inflation_radius, float &gear_speed);
  bool reachInFrontOfBalloon(std::vector<geometry_msgs::PoseStamped>& points_list, float &inflation_radius, float &gear_speed);
  bool behaviorBurstBalloon(std::vector<geometry_msgs::PoseStamped> & points_list,float & inflation_radius,float & gear_speed);

};
#endif
