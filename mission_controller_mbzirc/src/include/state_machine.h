#ifndef STATE_MACHINE_H
#define STATE_MACHINE_H

#include <iostream>
#include <math.h>
#include "eigen3/Eigen/Eigen"
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/PoseStamped.h>

namespace Item
{
  enum class State {
        START_STATE,
        TAKE_OFF,
        SEARCH,
        FOLLOW_DRONE_CLOSE,
        FOLLOW_DRONE_FAR,
        FOCUS,
        RECOVER,
        INTERCEPTION,
        CATCH_BALL,
        LAND,
        FOLLOW_BALL
  };

  enum class Event {
      UNKNOWN,
      TAKE_OFF,
      TAKE_OFF_COMPLETED,
      FAR_DRONE_DETECTED,
      CLOSE_DRONE_DETECTED,
      FOLLOW_COMPLETED,
      DRONE_DETECTION_LOST,
      BALL_DETECTED,
      BALL_DETECTION_LOST,
      SUCCESSFUL_CATCH,
      FAILED_CATCH,
      TIMEOUT_RECOVER,
      ARRIVED_TO_SEARCH,
      EXECUTE_CATCH
  };
}

class StateMachine
{

public:

  void setUp();

  void start();

  void stop();

  //Constructor.
  StateMachine();

  // Destructor.
  ~StateMachine();

  void nextState(Item::State & current_state, Item::Event event);
  void printState(Item::State state);
  bool checkTimeout(Item::State state);
  void activateBallDetectionAllTime(bool activate_ball_detector_all_time);


  //Timeouts

private:

  double state_timeout;
  bool ACTIVATE_BALL_DETECTOR_ALL_TIME;

};
#endif 
