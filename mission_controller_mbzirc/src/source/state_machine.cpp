#include "state_machine.h"

//Constructor
StateMachine::StateMachine(){

    ACTIVATE_BALL_DETECTOR_ALL_TIME = false;
}

//Destructor
StateMachine::~StateMachine() {}

//Returns next state given the current_state and an event
void StateMachine::nextState(Item::State & current_state, Item::Event event)
{
    switch(current_state){

        std::cout<< ACTIVATE_BALL_DETECTOR_ALL_TIME << std::endl;
        // ************* START_STATE *************
        case Item::State::START_STATE:
        	if (event == Item::Event::TAKE_OFF){
        		current_state = Item::State::TAKE_OFF;
                printState(current_state);
        	}
        break;
        // ************* TAKE_OFF *************

        case Item::State::TAKE_OFF:
        	if (event == Item::Event::TAKE_OFF_COMPLETED){
                        current_state = Item::State::FOCUS;
                printState(current_state);
        	}
        break;

        // ************* FOCUS *************

        case Item::State::FOCUS:
            if (event == Item::Event::ARRIVED_TO_SEARCH){
                current_state = Item::State::SEARCH;
                printState(current_state);
            }
            //if (event == Item::Event::FAR_DRONE_DETECTED){
            //    current_state = Item::State::FOLLOW_DRONE_FAR;
            //    printState(current_state);
            //}
            //if (event == Item::Event::CLOSE_DRONE_DETECTED){
            //    current_state = Item::State::FOLLOW_DRONE_CLOSE;
            //    printState(current_state);
            //}

        //if(ACTIVATE_BALL_DETECTOR_ALL_TIME){
            //if (event == Item::Event::BALL_DETECTED){
                //current_state = Item::State::FOLLOW_BALL;
                //printState(current_state);
            //}
        //}

        break;

        // ************* SEARCH *************

        case Item::State::SEARCH:
            if (event == Item::Event::FAR_DRONE_DETECTED){
                current_state = Item::State::FOLLOW_DRONE_FAR;
                printState(current_state);
            }
            if (event == Item::Event::CLOSE_DRONE_DETECTED){
                current_state = Item::State::FOLLOW_DRONE_CLOSE;
                printState(current_state);
            }
       if(ACTIVATE_BALL_DETECTOR_ALL_TIME){
                if (event == Item::Event::BALL_DETECTED){
                    current_state = Item::State::FOLLOW_BALL;
                    printState(current_state);
                }
       }

        break;

        // ************* FOLLOW_DRONE_FAR *************
        case Item::State::FOLLOW_DRONE_FAR:
            if (event == Item::Event::CLOSE_DRONE_DETECTED){
                current_state = Item::State::FOLLOW_DRONE_CLOSE;
                printState(current_state);
            }
            if (event == Item::Event::DRONE_DETECTION_LOST){
                current_state = Item::State::FOCUS;
                printState(current_state);
            }
       if(ACTIVATE_BALL_DETECTOR_ALL_TIME){
            if (event == Item::Event::BALL_DETECTED){
                current_state = Item::State::FOLLOW_BALL;
                printState(current_state);
            }
       }

        break;

        // ************* FOLLOW_DRONE_CLOSE *************
        case Item::State::FOLLOW_DRONE_CLOSE:
            if (event == Item::Event::FOLLOW_COMPLETED){
                current_state = Item::State::INTERCEPTION;
                printState(current_state);
            }
            if (event == Item::Event::DRONE_DETECTION_LOST){
                current_state = Item::State::RECOVER;
		//current_state = Item::State::FOCUS;
                printState(current_state);
            }
        if(ACTIVATE_BALL_DETECTOR_ALL_TIME){
            if (event == Item::Event::BALL_DETECTED){
                 current_state = Item::State::FOLLOW_BALL;
                 printState(current_state);
            }
        }
        break;
        case Item::State::RECOVER:
        // ************* RECOVER *************

            if (event == Item::Event::TIMEOUT_RECOVER){
                current_state = Item::State::FOCUS;
                printState(current_state);
            }
            //if (event == Item::Event::CLOSE_DRONE_DETECTED){
            //    current_state = Item::State::FOLLOW_DRONE_CLOSE;
            //    printState(current_state);
            //}
            //if (event == Item::Event::FAR_DRONE_DETECTED){
            //    current_state = Item::State::FOLLOW_DRONE_FAR;
            //    printState(current_state);
            //}
       //if(ACTIVATE_BALL_DETECTOR_ALL_TIME){
            //if (event == Item::Event::BALL_DETECTED){
            //    current_state = Item::State::FOLLOW_BALL;
            //    printState(current_state);
            //}
       //}
                current_state = Item::State::FOCUS;
                printState(current_state);

        break;
          // ************* INTERCEPTION *************

//        case Item::State::INTERCEPTION:
//            if (event == Item::Event::CLOSE_BALL_DETECTED){
//                current_state = Item::State::CATCH_BALL;
//                printState(current_state);
//            }
//            if (event == Item::Event::BALL_DETECTION_LOST){
//                current_state = Item::State::SEARCH;
//                printState(current_state);
//            }
        break;
        // ************* FOLLOW_BALL *************
        case Item::State::FOLLOW_BALL:
            if (event == Item::Event::EXECUTE_CATCH){
                current_state = Item::State::CATCH_BALL;
                printState(current_state);
            }
            if (event == Item::Event::BALL_DETECTION_LOST){
                current_state = Item::State::RECOVER;
                printState(current_state);
            }
        break;
        // ************* CATCH_BALL *************
        case Item::State::CATCH_BALL:
            if (event == Item::Event::SUCCESSFUL_CATCH){
                current_state = Item::State::LAND;
                printState(current_state);
            }
            if (event == Item::Event::FAILED_CATCH){
                current_state = Item::State::FOCUS;
                printState(current_state);
            }
        break;
        // ************* LAND *************

        case Item::State::LAND:
            if (event == Item::Event::TAKE_OFF){
                current_state = Item::State::TAKE_OFF;
                printState(current_state);
            }
        break;
    }
}

//Check timeout 
//bool StateMachine::checkTimeout(Item::State state){
//    switch(state){
//        case Item::State::START_STATE:
//            //if (fabs(ros::Time::now().toSec() - state_timeout) >= start_state_timeout) return true;
//        break;
//        case Item::State::TAKE_OFF:
//            if (fabs(ros::Time::now().toSec() - state_timeout) >= takeoff_state_timeout) return true;
//        break;
//        case Item::State::SEARCH:
//            if (fabs(ros::Time::now().toSec() - state_timeout) >= searching_state_timeout) return true;
//        break;
//        case Item::State::FOLLOW_DRONE_CLOSE:
//            if (fabs(ros::Time::now().toSec() - state_timeout) >= followclose_state_timeout) return true;
//        break;
//        case Item::State::FOLLOW_DRONE_FAR:
//            if (fabs(ros::Time::now().toSec() - state_timeout) >= followfar_state_timeout) return true;
//        break;
//        case Item::State::RECOVER:
//            if (fabs(ros::Time::now().toSec() - state_timeout) >= recover_state_timeout) return true;
//        break;
//        case Item::State::INTERCEPTION:
//            if (fabs(ros::Time::now().toSec() - state_timeout) >= interception_state_timeout) return true;
//        break;
//        case Item::State::CATCH_BALL:
//            if (fabs(ros::Time::now().toSec() - state_timeout) >= catchball_state_timeout) return true;
//        break;
//        case Item::State::LAND:
//            if (fabs(ros::Time::now().toSec() - state_timeout) >= land_state_timeout) return true;
//        break;
//    }
//    return false;
//}

void StateMachine::printState(Item::State state){
    switch(state){
        case Item::State::START_STATE:
            std::cout << "\033[1;92m[State] START_STATE\033[0m" << std::endl;
        break;
        case Item::State::TAKE_OFF:
            std::cout << "\033[1;92m[State] TAKE_OFF\033[0m" << std::endl;
        break;
        case Item::State::SEARCH:
            std::cout << "\033[1;92m[State] SEARCHING\033[0m" << std::endl;
        break;
        case Item::State::FOLLOW_DRONE_CLOSE:
            std::cout << "\033[1;92m[State] FOLLOW_DRONE_CLOSE\033[0m" << std::endl;
        break;
        case Item::State::FOLLOW_DRONE_FAR:
            std::cout << "\033[1;92m[State] FOLLOW_DRONE_FAR\033[0m" << std::endl;
        break;
        case Item::State::RECOVER:
            std::cout << "\033[1;92m[State] RECOVER\033[0m" << std::endl;
        break;
        case Item::State::INTERCEPTION:
            std::cout << "\033[1;92m[State] INTERCEPTION\033[0m" << std::endl;
        break;
        case Item::State::CATCH_BALL:
            std::cout << "\033[1;92m[State] CATCH_BALL\033[0m" << std::endl;
        break;
        case Item::State::LAND:
            std::cout << "\033[1;92m[State] LAND\033[0m" << std::endl;
        break;
        case Item::State::FOCUS:
            std::cout << "\033[1;92m[State] FOCUS\033[0m" << std::endl;
        break;
        case Item::State::FOLLOW_BALL:
            std::cout << "\033[1;92m[State] FOLLOW BALL\033[0m" << std::endl;
        break;

    }    
}

void StateMachine::activateBallDetectionAllTime(bool activate_ball_detector_all_time){

    ACTIVATE_BALL_DETECTOR_ALL_TIME = activate_ball_detector_all_time;
}
