#include <iostream>
#include <math.h>

#include "ros/ros.h"
#include "balloon/mission_controller_balloon_mbzirc_process.h"
#include "nodes_definition.h"

int main(int argc,char **argv)
{
    //Ros Init
    ros::init(argc, argv, ros::this_node::getName());
    ros::NodeHandle n;

    std::cout << "[ROSNODE] Starting "<<ros::this_node::getName() << std::endl;
       
    MissionControllerBalloonProcess missionController;
    missionController.setUp();
    missionController.start();

    //Waiting for topics to be ready
    
    ros::Rate loop_rate(missionController.get_moduleRate());

    ros::Duration(3).sleep();

    dynamic_reconfigure::Server<mission_controller_mbzirc::MyParamsConfig> server;
    dynamic_reconfigure::Server<mission_controller_mbzirc::MyParamsConfig>::CallbackType f;

    f = boost::bind(&MissionControllerBalloonProcess::dynamicTunningCallback,&missionController, _1, _2);

   server.setCallback(f);
    try
    {

        while(ros::ok())
        {  
            if(missionController.getState()==STATE_RUNNING){
                missionController.run();
            } 
             ros::spinOnce();
             loop_rate.sleep();
        }
    }
    catch (std::exception &ex)
    {
        std::cout << "[ROSNODE] Exception :" << ex.what() << std::endl;
    }
    return 0;
}
