#ifndef STATE_MACHINE_BALLOON_H
#define STATE_MACHINE_BALLOON_H

#include <iostream>
#include <math.h>
#include "eigen3/Eigen/Eigen"
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/PoseStamped.h>

namespace ItemBalloon
{
enum class State {
  START_STATE,
  TAKE_OFF,
  SEARCH_BALLOON,
  BURST_BALLOON,
  LAND
};

enum class Event {
  UNKNOWN,
  TAKE_OFF,
  TAKE_OFF_COMPLETED,
  BALLOON_DETECTED,
  SUCCESSFUL_BURST,
  FAILED_BURST,
  SEARCH_COMPLETE
};
}

class StateMachineBalloon
{

public:

  void setUp();

  void start();

  void stop();

  //Constructor.
  StateMachineBalloon();

  // Destructor.
  ~StateMachineBalloon();

  void nextState(ItemBalloon::State & current_state, ItemBalloon::Event event);
  void printState(ItemBalloon::State state);
  bool checkTimeout(ItemBalloon::State state);
  void activateBallDetectionAllTime(bool activate_ball_detector_all_time);


  //Timeouts

private:

  double state_timeout;
  bool ACTIVATE_BALL_DETECTOR_ALL_TIME;

};
#endif
